//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BankApplication.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class BankUserTb
    {
        public int UserNo { get; set; }
        public string UserName { get; set; }
        public Nullable<int> Age { get; set; }
        public string Address { get; set; }
        public string ContactNumber { get; set; }
        public string Status { get; set; }
        public string Role { get; set; }
        public string email { get; set; }
        public string password { get; set; }
    }
}
