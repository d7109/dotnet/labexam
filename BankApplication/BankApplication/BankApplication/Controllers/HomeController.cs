﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BankApplication.Models;
using System.Web.Security;

namespace BankApplication.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        BankDBEntities dbhelper = new BankDBEntities();
        
        [Authorize]
        public ActionResult Index()
        {
            return View( "AllUser" , dbhelper.BankUserTbs.ToList());
        }

        public ActionResult SighIn()
        {
            return View("SighIn");
        }

        public ActionResult AfterSighin( BankUserTb user, String returnurl)
        {
            if (CheckWithDatatbase(user))
            {
                FormsAuthentication.SetAuthCookie(user.UserName, false);
                if (returnurl != null)
                {
                    return Redirect(returnurl);
                }
                else
                {

                    return Redirect("/Home/Index");
                }


            }
            else
            {
                ViewBag.message = "password or email incorrect";
                return View("SighIn");
            }
        }
      
        private bool CheckWithDatatbase(BankUserTb PersonToBeValidated)
        {
            foreach (BankUserTb item in dbhelper.BankUserTbs)
            {
                if (item.email == PersonToBeValidated.email && item.password == PersonToBeValidated.password)
                    return true;
            }

            return false;
        }
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return Redirect("SighIn");
        }

        public ActionResult Create()
        {
            return View("Create");

        }
      
        public ActionResult AfterCreate(BankUserTb NewUser)
        {

            dbhelper.BankUserTbs.Add(NewUser);
            int rowsaffected = dbhelper.SaveChanges();
            if (rowsaffected > 0)
            {
                return Redirect("/Home/Index");
            }
            else
            {

                return View("Create");
            }
        }
        public ActionResult Edit(int id)
        {
            BankUserTb UserToBeEdited = dbhelper.BankUserTbs.Find(id);

            return View("Edit", UserToBeEdited);

        }

        public ActionResult AfterEdit(BankUserTb UserToBeUpdated)
        {
            BankUserTb UserToBEdited = dbhelper.BankUserTbs.Find(UserToBeUpdated.UserNo);

            UserToBEdited.Status = UserToBeUpdated.Status;
            //RationCardToBeEdited.Address = RationCardToBeUpdated.Address;

            int rowsAffected = dbhelper.SaveChanges();

            if (rowsAffected > 0)
            {
                return Redirect("/Home/Index");

            }
            else
            {
                return View("Create");
            }










        }
    }
}